import 'package:flutter/material.dart';

class MovieSummary {
  MovieSummary({
    @required this.id,
    @required this.voteAverage,
    @required this.title,
    @required this.posterUrl,
    @required this.isFavorite,
  })  : assert(id != null),
        assert(voteAverage != null),
        assert(title != null),
        assert(posterUrl != null),
        assert(isFavorite != null);

  final int id;
  final double voteAverage;
  final String title;
  final String posterUrl;
  bool isFavorite;
}