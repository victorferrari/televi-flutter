import 'package:flutter/widgets.dart';

class MovieDetail {
  MovieDetail({
    @required this.id,
    @required this.voteAverage,
    @required this.title,
    @required this.genres,
    @required this.releaseDate,
    @required this.runtime,
    @required this.overview,
    @required this.posterUrl,
  })  : assert(id != null),
        assert(voteAverage != null),
        assert(title != null),
        assert(genres != null),
        assert(releaseDate != null),
        assert(runtime != null),
        assert(overview != null),
        assert(posterUrl != null);

  final int id;
  final double voteAverage;
  final String title;
  final String posterUrl;
  final List<String> genres;
  final String releaseDate;
  final int runtime;
  final String overview;
}
