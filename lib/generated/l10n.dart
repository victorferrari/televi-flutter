// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `An unexpected error has occurred`
  String get generalExceptionMessage {
    return Intl.message(
      'An unexpected error has occurred',
      name: 'generalExceptionMessage',
      desc: '',
      args: [],
    );
  }

  /// `No Internet connection`
  String get noInternetExceptionMessage {
    return Intl.message(
      'No Internet connection',
      name: 'noInternetExceptionMessage',
      desc: '',
      args: [],
    );
  }

  /// `Try again`
  String get emptyStateTryAgain {
    return Intl.message(
      'Try again',
      name: 'emptyStateTryAgain',
      desc: '',
      args: [],
    );
  }

  /// `Movies`
  String get bottomNavigationMoviesTab {
    return Intl.message(
      'Movies',
      name: 'bottomNavigationMoviesTab',
      desc: '',
      args: [],
    );
  }

  /// `Favorites`
  String get bottomNavigationFavoritesTab {
    return Intl.message(
      'Favorites',
      name: 'bottomNavigationFavoritesTab',
      desc: '',
      args: [],
    );
  }

  /// `Movies`
  String get movieListAppBarTitle {
    return Intl.message(
      'Movies',
      name: 'movieListAppBarTitle',
      desc: '',
      args: [],
    );
  }

  /// `Favorites`
  String get favoriteListAppBarTitle {
    return Intl.message(
      'Favorites',
      name: 'favoriteListAppBarTitle',
      desc: '',
      args: [],
    );
  }

  /// `Title`
  String get movieDetailTitleLabel {
    return Intl.message(
      'Title',
      name: 'movieDetailTitleLabel',
      desc: '',
      args: [],
    );
  }

  /// `Overview`
  String get movieDetailOverviewLabel {
    return Intl.message(
      'Overview',
      name: 'movieDetailOverviewLabel',
      desc: '',
      args: [],
    );
  }

  /// `Genres`
  String get movieDetailGenresLabel {
    return Intl.message(
      'Genres',
      name: 'movieDetailGenresLabel',
      desc: '',
      args: [],
    );
  }

  /// `Release date`
  String get movieDetailReleaseDateLabel {
    return Intl.message(
      'Release date',
      name: 'movieDetailReleaseDateLabel',
      desc: '',
      args: [],
    );
  }

  /// `Runtime`
  String get movieDetailRuntimeLabel {
    return Intl.message(
      'Runtime',
      name: 'movieDetailRuntimeLabel',
      desc: '',
      args: [],
    );
  }

  /// `{duration} min`
  String movieDetailRuntimeText(Object duration) {
    return Intl.message(
      '$duration min',
      name: 'movieDetailRuntimeText',
      desc: '',
      args: [duration],
    );
  }

  /// `Vote average`
  String get movieDetailVoteAverageLabel {
    return Intl.message(
      'Vote average',
      name: 'movieDetailVoteAverageLabel',
      desc: '',
      args: [],
    );
  }

  /// `MM/dd/yyyy`
  String get dateFormat {
    return Intl.message(
      'MM/dd/yyyy',
      name: 'dateFormat',
      desc: '',
      args: [],
    );
  }

  /// `Movie successfully added`
  String get toggleFavoriteSuccessAddMessage {
    return Intl.message(
      'Movie successfully added',
      name: 'toggleFavoriteSuccessAddMessage',
      desc: '',
      args: [],
    );
  }

  /// `Movie successfully removed`
  String get toggleFavoriteSuccessRemoveMessage {
    return Intl.message(
      'Movie successfully removed',
      name: 'toggleFavoriteSuccessRemoveMessage',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en', countryCode: 'US'),
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'pt', countryCode: 'BR'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}