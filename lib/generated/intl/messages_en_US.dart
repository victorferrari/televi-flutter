// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en_US locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en_US';

  static m0(duration) => "${duration} min";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "bottomNavigationFavoritesTab" : MessageLookupByLibrary.simpleMessage("Favorites"),
    "bottomNavigationMoviesTab" : MessageLookupByLibrary.simpleMessage("Movies"),
    "dateFormat" : MessageLookupByLibrary.simpleMessage("MM/dd/yyyy"),
    "emptyStateTryAgain" : MessageLookupByLibrary.simpleMessage("Try again"),
    "favoriteListAppBarTitle" : MessageLookupByLibrary.simpleMessage("Favorites"),
    "generalExceptionMessage" : MessageLookupByLibrary.simpleMessage("An unexpected error has occurred"),
    "movieDetailGenresLabel" : MessageLookupByLibrary.simpleMessage("Genres"),
    "movieDetailOverviewLabel" : MessageLookupByLibrary.simpleMessage("Overview"),
    "movieDetailReleaseDateLabel" : MessageLookupByLibrary.simpleMessage("Release date"),
    "movieDetailRuntimeLabel" : MessageLookupByLibrary.simpleMessage("Runtime"),
    "movieDetailRuntimeText" : m0,
    "movieDetailTitleLabel" : MessageLookupByLibrary.simpleMessage("Title"),
    "movieDetailVoteAverageLabel" : MessageLookupByLibrary.simpleMessage("Vote average"),
    "movieListAppBarTitle" : MessageLookupByLibrary.simpleMessage("Movies"),
    "noInternetExceptionMessage" : MessageLookupByLibrary.simpleMessage("No Internet connection"),
    "toggleFavoriteSuccessAddMessage" : MessageLookupByLibrary.simpleMessage("Movie successfully added"),
    "toggleFavoriteSuccessRemoveMessage" : MessageLookupByLibrary.simpleMessage("Movie successfully removed")
  };
}
