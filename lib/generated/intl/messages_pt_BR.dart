// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a pt_BR locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'pt_BR';

  static m0(duration) => "${duration} min";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "bottomNavigationFavoritesTab" : MessageLookupByLibrary.simpleMessage("Favoritos"),
    "bottomNavigationMoviesTab" : MessageLookupByLibrary.simpleMessage("Filmes"),
    "dateFormat" : MessageLookupByLibrary.simpleMessage("dd/MM/yyyy"),
    "emptyStateTryAgain" : MessageLookupByLibrary.simpleMessage("Tente novamente"),
    "favoriteListAppBarTitle" : MessageLookupByLibrary.simpleMessage("Favoritos"),
    "generalExceptionMessage" : MessageLookupByLibrary.simpleMessage("Um erro inesperado ocorreu"),
    "movieDetailGenresLabel" : MessageLookupByLibrary.simpleMessage("Gêneros"),
    "movieDetailOverviewLabel" : MessageLookupByLibrary.simpleMessage("Sinopse"),
    "movieDetailReleaseDateLabel" : MessageLookupByLibrary.simpleMessage("Data de lançamento"),
    "movieDetailRuntimeLabel" : MessageLookupByLibrary.simpleMessage("Duração"),
    "movieDetailRuntimeText" : m0,
    "movieDetailTitleLabel" : MessageLookupByLibrary.simpleMessage("Título"),
    "movieDetailVoteAverageLabel" : MessageLookupByLibrary.simpleMessage("Nota média"),
    "movieListAppBarTitle" : MessageLookupByLibrary.simpleMessage("Filmes"),
    "noInternetExceptionMessage" : MessageLookupByLibrary.simpleMessage("Sem conexão com a Internet"),
    "toggleFavoriteSuccessAddMessage" : MessageLookupByLibrary.simpleMessage("Filme adicionado com sucesso"),
    "toggleFavoriteSuccessRemoveMessage" : MessageLookupByLibrary.simpleMessage("Filme removido com sucesso")
  };
}
