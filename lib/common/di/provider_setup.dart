import 'package:dio/dio.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:televi_flutter/data/cache/movie_cache_data_source.dart';
import 'package:televi_flutter/data/remote/movie_remote_data_source.dart';
import 'package:televi_flutter/data/repository/movie_repository.dart';
import 'package:televi_flutter/navigation/route_name_builder.dart';
import 'package:televi_flutter/presentation/screens/favorite_list/favorite_list_page.dart';
import 'package:televi_flutter/presentation/screens/home/home_screen.dart';
import 'package:televi_flutter/presentation/screens/movie_detail/movie_detail_page.dart';
import 'package:televi_flutter/presentation/screens/movie_list/movie_list_page.dart';

List<SingleChildWidget> globalProviders = [
  ..._buildRemoteProviders,
  ..._buildCacheProviders,
  ..._buildRepositoryProviders,
  ..._buildRouterProviders,
];

List<SingleChildWidget> _buildRemoteProviders = [
  Provider<Dio>(
    create: (_) => Dio(),
  ),
  ProxyProvider<Dio, MovieRemoteDataSource>(
    update: (_, dio, __) => MovieRemoteDataSource(dio: dio),
  ),
];

List<SingleChildWidget> _buildCacheProviders = [
  Provider<MovieCacheDataSource>(
    create: (_) => MovieCacheDataSource(),
  ),
];

List<SingleChildWidget> _buildRepositoryProviders = [
  ProxyProvider2<MovieCacheDataSource, MovieRemoteDataSource, MovieRepository>(
    update: (_, cache, remote, __) => MovieRepository(
      remote: remote,
      cache: cache,
    ),
  ),
];

List<SingleChildWidget> _buildRouterProviders = [
  Provider<FluroRouter>(
    create: (_) => FluroRouter.appRouter
      ..define(
        RouteNameBuilder.homePath,
        handler: Handler(
          handlerFunc: (_, __) => HomeScreen(),
        ),
      )
      ..define(
        RouteNameBuilder.movieListPath,
        handler: Handler(
          handlerFunc: (context, _) => MovieListPage.create(),
        ),
      )
      ..define(
        '${RouteNameBuilder.movieDetailPath}/:${RouteNameBuilder.movieDetailIdParameter}',
        handler: Handler(
          handlerFunc: (context, params) {
            final id =
                int.parse(params[RouteNameBuilder.movieDetailIdParameter][0]);
            final title = params[RouteNameBuilder.movieDetailTitleParameter][0];
            return MovieDetailPage.create(
              movieId: id,
              movieTitle: title,
            );
          },
        ),
      )
      ..define(
        RouteNameBuilder.favoriteListPath,
        handler: Handler(
          handlerFunc: (context, _) =>
              FavoriteListPage.create(),
        ),
      ),
  ),
  ProxyProvider<FluroRouter, RouteFactory>(
    update: (context, router, _) => (settings) => router
        .matchRoute(context, settings.name, routeSettings: settings)
        .route,
  ),
];
