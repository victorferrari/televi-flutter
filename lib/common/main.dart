import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:televi_flutter/common/di/provider_setup.dart';
import 'package:televi_flutter/data/cache/models/movie_detail_cm.dart';
import 'package:televi_flutter/data/cache/models/movie_summary_cm.dart';
import 'package:televi_flutter/generated/l10n.dart';
import 'package:televi_flutter/presentation/screens/home/home_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  Hive
    ..init((await getApplicationDocumentsDirectory()).path)
    ..registerAdapter<MovieSummaryCM>(MovieSummaryCMAdapter())
    ..registerAdapter<MovieDetailCM>(MovieDetailCMAdapter());

  runApp(
    MultiProvider(
      providers: globalProviders,
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Platform.isAndroid
      ? MaterialApp(
          title: 'Televi-Flutter',
          theme: ThemeData(
            primarySwatch: Colors.purple,
            accentColor: Colors.deepOrange,
            fontFamily: 'Raleway',
            textTheme: const TextTheme(
              headline6: TextStyle(
                  fontFamily: 'RobotoCondensed',
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
              headline5: TextStyle(
                  fontFamily: 'RobotoCondensed',
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                  color: Colors.purple),
              bodyText1: TextStyle(
                  fontFamily: 'Raleway',
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
              bodyText2: TextStyle(
                  fontFamily: 'Raleway',
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                  color: Colors.blueGrey),
              button: TextStyle(),
            ),
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: HomeScreen(),
          localizationsDelegates: [
            S.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: S.delegate.supportedLocales,
          onGenerateRoute: Provider.of<RouteFactory>(context),
        )
      : CupertinoApp(
          theme: const CupertinoThemeData(
            primaryColor: Color(0xFF9C27B0),
            textTheme: CupertinoTextThemeData(
              tabLabelTextStyle: TextStyle(
                  fontFamily: 'RobotoCondensed',
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
              textStyle: TextStyle(
                  fontFamily: 'Raleway',
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),
            ),
          ),
          home: HomeScreen(),
          localizationsDelegates: [
            S.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          onGenerateRoute: Provider.of<RouteFactory>(context),
        );
}
