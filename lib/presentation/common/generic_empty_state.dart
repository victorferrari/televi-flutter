import 'package:flutter/material.dart';
import 'package:televi_flutter/generated/l10n.dart';
import 'package:televi_flutter/presentation/common/empty_state.dart';

class GenericEmptyState extends StatelessWidget {
  const GenericEmptyState({
    @required this.onTapTryAgain,
  }) : assert(onTapTryAgain != null);

  final VoidCallback onTapTryAgain;

  @override
  Widget build(BuildContext context) => EmptyState(
      message: S.of(context).generalExceptionMessage,
      asset: 'assets/images/general-error.png',
      onTapTryAgain: onTapTryAgain);
}
