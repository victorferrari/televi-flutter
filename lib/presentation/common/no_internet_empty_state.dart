import 'package:flutter/material.dart';
import 'package:televi_flutter/generated/l10n.dart';
import 'package:televi_flutter/presentation/common/empty_state.dart';

class NoInternetEmptyState extends StatelessWidget {
  const NoInternetEmptyState({
    @required this.onTapTryAgain,
  }) : assert(onTapTryAgain != null);

  final VoidCallback onTapTryAgain;

  @override
  Widget build(BuildContext context) => EmptyState(
      message: S.of(context).noInternetExceptionMessage,
      asset: 'assets/images/connection-lost.png',
      onTapTryAgain: onTapTryAgain);
}