import 'package:flutter/material.dart';
import 'package:televi_flutter/generated/l10n.dart';

class EmptyState extends StatelessWidget {
  const EmptyState({
    @required this.message,
    @required this.asset,
    @required this.onTapTryAgain,
  })  : assert(message != null),
        assert(asset != null),
        assert(onTapTryAgain != null);

  final String message;
  final String asset;
  final VoidCallback onTapTryAgain;

  @override
  Widget build(BuildContext context) => Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(asset),
            Text(message, style: Theme.of(context).textTheme.bodyText2),
            const SizedBox(height: 30),
            FlatButton(
              onPressed: onTapTryAgain,
              child: Text(
                S.of(context).emptyStateTryAgain,
                style: const TextStyle(
                    color: Colors.lightBlue,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      );
}
