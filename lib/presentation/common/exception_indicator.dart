import 'package:flutter/widgets.dart';
import 'package:televi_flutter/data/remote/exceptions.dart';
import 'package:televi_flutter/presentation/common/generic_empty_state.dart';
import 'package:televi_flutter/presentation/common/no_internet_empty_state.dart';

class ExceptionIndicator extends StatelessWidget {
  const ExceptionIndicator({
    @required this.exception,
    @required this.onTapTryAgain,
  })  : assert(exception != null),
        assert(onTapTryAgain != null);

  final dynamic exception;
  final VoidCallback onTapTryAgain;

  @override
  Widget build(BuildContext context) => exception is NoInternetException
      ? NoInternetEmptyState(
          onTapTryAgain: onTapTryAgain,
        )
      : GenericEmptyState(
          onTapTryAgain: onTapTryAgain,
        );
}
