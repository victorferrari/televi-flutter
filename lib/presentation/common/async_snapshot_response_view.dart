import 'package:flutter/material.dart';
import 'package:televi_flutter/data/remote/exceptions.dart';
import 'package:televi_flutter/presentation/common/loading_progress.dart';

class AsyncSnapshotResponseView<Loading, Error, Success>
    extends StatelessWidget {
  AsyncSnapshotResponseView({
    @required this.successWidgetBuilder,
    @required this.errorWidgetBuilder,
    @required this.snapshot,
    this.onTryAgainTap,
    Key key,
  })  : assert(successWidgetBuilder != null),
        assert(errorWidgetBuilder != null),
        assert(snapshot != null),
        assert(Loading != dynamic),
        assert(Error != dynamic),
        assert(Success != dynamic),
        super(key: key);

  final AsyncSnapshot snapshot;
  final GestureTapCallback onTryAgainTap;
  final Widget Function(BuildContext context, Success success)
      successWidgetBuilder;
  final Widget Function(BuildContext context, Error error) errorWidgetBuilder;

  @override
  Widget build(BuildContext context) {
    final snapshotData = snapshot.data;
    if (snapshotData == null || snapshotData is Loading) {
      return LoadingProgress();
    }

    if (snapshotData is Error) {
      return errorWidgetBuilder(context, snapshotData);
    }

    if (snapshotData is Success) {
      return successWidgetBuilder(context, snapshotData);
    }

    throw UnknownStateTypeException();
  }
}

class UnknownStateTypeException implements Exception {}
