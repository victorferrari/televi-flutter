import 'dart:async';

import 'package:flutter/widgets.dart';

class ActionHandler<T> extends StatefulWidget {
  const ActionHandler(
      {@required this.stream,
      @required this.onAction,
      @required this.child,
      Key key})
      : assert(stream != null),
        assert(onAction != null),
        assert(child != null),
        super(key: key);

  final Stream<T> stream;
  final ValueChanged<T> onAction;
  final Widget child;

  @override
  _ActionHandlerState createState() => _ActionHandlerState();
}

class _ActionHandlerState extends State<ActionHandler> {
  StreamSubscription subscription;

  @override
  void didChangeDependencies() {
    subscription ??= widget.stream.listen(widget.onAction);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) => widget.child;

  @override
  void dispose() {
    subscription.cancel();
    super.dispose();
  }
}
