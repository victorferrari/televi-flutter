import 'package:flutter/widgets.dart';

class AppFlow {
  AppFlow({
    @required this.title,
    @required this.icon,
    @required this.navigatorKey,
    @required this.initialRouteName,
  })  : assert(title != null),
        assert(icon != null),
        assert(navigatorKey != null),
        assert(initialRouteName != null);

  final String title;
  final Icon icon;
  final GlobalKey<NavigatorState> navigatorKey;
  final String initialRouteName;
}
