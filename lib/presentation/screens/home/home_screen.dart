import 'package:flutter/material.dart';
import 'package:televi_flutter/navigation/route_name_builder.dart';
import 'package:televi_flutter/presentation/screens/home/app_flow.dart';
import 'package:televi_flutter/presentation/screens/home/bottom_navigation/adaptive_bottom_navigation_scaffold.dart';
import 'package:televi_flutter/presentation/screens/home/bottom_navigation/bottom_navigation_tab.dart';
import 'package:televi_flutter/generated/l10n.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  var _flows = [];

  @override
  void didChangeDependencies() {
    _flows = <AppFlow>[
      AppFlow(
        title: S.of(context).bottomNavigationMoviesTab,
        icon: const Icon(Icons.local_movies),
        navigatorKey: GlobalKey<NavigatorState>(),
        initialRouteName: RouteNameBuilder.movieListPath,
      ),
      AppFlow(
        title: S.of(context).bottomNavigationFavoritesTab,
        icon: const Icon(Icons.favorite),
        navigatorKey: GlobalKey<NavigatorState>(),
        initialRouteName: RouteNameBuilder.favoriteListPath,
      )
    ];
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) => AdaptiveBottomNavigationScaffold(
        navigationBarItems: _flows
            .map(
              (flow) => BottomNavigationTab(
                bottomNavigationBarItem: BottomNavigationBarItem(
                  label: flow.title,
                  icon: flow.icon,
                ),
                navigatorKey: flow.navigatorKey,
                initialRouteName: flow.initialRouteName,
              ),
            )
            .toList(),
      );
}
