import 'package:flutter/material.dart';
import 'package:televi_flutter/navigation/page_flow.dart';
import 'package:televi_flutter/presentation/screens/home/bottom_navigation/bottom_navigation_tab.dart';
import 'package:televi_flutter/presentation/screens/home/bottom_navigation/material/material_bottom_navigation_tab.dart';

/// A Scaffold with a configured BottomNavigationBar, separate
/// Navigators for each tab view and state retaining across tab switches.
class MaterialBottomNavigationScaffold extends StatefulWidget {
  const MaterialBottomNavigationScaffold({
    @required this.navigationBarItems,
    @required this.onItemSelected,
    @required this.selectedIndex,
    Key key,
  })  : assert(navigationBarItems != null),
        assert(onItemSelected != null),
        assert(selectedIndex != null),
        super(key: key);

  final List<BottomNavigationTab> navigationBarItems;
  final ValueChanged<int> onItemSelected;
  final int selectedIndex;

  @override
  _MaterialBottomNavigationScaffoldState createState() =>
      _MaterialBottomNavigationScaffoldState();
}

class _MaterialBottomNavigationScaffoldState
    extends State<MaterialBottomNavigationScaffold>
    with TickerProviderStateMixin<MaterialBottomNavigationScaffold> {
  final List<MaterialBottomNavigationTab> _materialNavigationBarItems = [];
  final List<AnimationController> _animationControllers = [];

  /// Controls which tabs should have its content built. This enables us to
  /// lazy instantiate it.
  final List<bool> _shouldBuildTab = <bool>[];

  @override
  void initState() {
    _initAnimationControllers();
    _initMaterialNavigationBarItems();

    _shouldBuildTab.addAll(
      List<bool>.filled(
        widget.navigationBarItems.length,
        false,
      ),
    );

    super.initState();
  }

  void _initMaterialNavigationBarItems() {
    _materialNavigationBarItems.addAll(
      widget.navigationBarItems
          .map(
            (barItem) => MaterialBottomNavigationTab(
              bottomNavigationBarItem: barItem.bottomNavigationBarItem,
              navigatorKey: barItem.navigatorKey,
              subtreeKey: GlobalKey(),
              initialRouteName: barItem.initialRouteName,
            ),
          )
          .toList(),
    );
  }

  void _initAnimationControllers() {
    _animationControllers.addAll(
      widget.navigationBarItems.map<AnimationController>(
        (destination) => AnimationController(
          vsync: this,
          duration: const Duration(milliseconds: 500),
        ),
      ),
    );

    if (_animationControllers.isNotEmpty) {
      _animationControllers[0].value = 1.0;
    }
  }

  @override
  void dispose() {
    _animationControllers.forEach(
      (controller) => controller.dispose(),
    );

    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        // The Stack is what allows us to retain state across tab
        // switches by keeping all of our views in the widget tree.
        body: Stack(
          fit: StackFit.expand,
          children: _materialNavigationBarItems
              .map(
                (barItem) => buildMaterialPageFlow(
                    barItem,
                    widget.selectedIndex,
                    _materialNavigationBarItems.indexOf(barItem),
                    _shouldBuildTab,
                    _animationControllers),
              )
              .toList(),
        ),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: widget.selectedIndex,
          unselectedItemColor: Colors.white,
          selectedItemColor: Colors.amber,
          selectedFontSize: 18,
          backgroundColor: Theme.of(context).primaryColor,
          items: widget.navigationBarItems
              .map(
                (item) => item.bottomNavigationBarItem,
              )
              .toList(),
          onTap: widget.onItemSelected,
        ),
      );
}
