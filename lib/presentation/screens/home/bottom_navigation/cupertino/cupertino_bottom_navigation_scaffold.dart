import 'package:flutter/cupertino.dart';
import 'package:televi_flutter/navigation/page_flow.dart';
import 'package:televi_flutter/presentation/screens/home/bottom_navigation/bottom_navigation_tab.dart';

class CupertinoBottomNavigationScaffold extends StatelessWidget {
  const CupertinoBottomNavigationScaffold({
    @required this.navigationBarItems,
    @required this.onItemSelected,
    @required this.selectedIndex,
    Key key,
  })  : assert(navigationBarItems != null),
        assert(onItemSelected != null),
        assert(selectedIndex != null),
        super(key: key);

  final List<BottomNavigationTab> navigationBarItems;
  final ValueChanged<int> onItemSelected;
  final int selectedIndex;

  @override
  Widget build(BuildContext context) => CupertinoTabScaffold(
        // As we're managing the selected index outside, there's no need
        // to make this Widget stateful. We just need pass the selectedIndex to
        // the controller every time the widget is rebuilt.
        controller: CupertinoTabController(initialIndex: selectedIndex),
        tabBuilder: (context, index) {
          final barItem = navigationBarItems[index];
          return buildCupertinoPageFlow(barItem, context);
        },
        tabBar: CupertinoTabBar(
          activeColor: CupertinoColors.activeOrange,
          inactiveColor: CupertinoColors.white,
          backgroundColor: CupertinoTheme.of(context).primaryColor,
          items: navigationBarItems
              .map(
                (item) => item.bottomNavigationBarItem,
              )
              .toList(),
          onTap: onItemSelected,
        ),
      );
}
