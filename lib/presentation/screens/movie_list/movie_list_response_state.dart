import 'package:flutter/widgets.dart';
import 'package:televi_flutter/models/movie_summary.dart';

abstract class MovieListResponseState {}

class Loading implements MovieListResponseState {}

class Success implements MovieListResponseState {
  Success({
    @required this.movieList,
  })  : assert(movieList != null);

  List<MovieSummary> movieList;
}

class Error implements MovieListResponseState {
  Error(this.error) : assert(error != null);

  dynamic error;
}
