import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:televi_flutter/data/repository/movie_repository.dart';
import 'package:televi_flutter/generated/l10n.dart';
import 'package:televi_flutter/presentation/common/action_handler.dart';
import 'package:televi_flutter/presentation/common/async_snapshot_response_view.dart';
import 'package:televi_flutter/presentation/screens/movie_list/toggle_favorite_movie/toggle_favorite_movie_bloc.dart';
import 'package:televi_flutter/presentation/screens/movie_list/toggle_favorite_movie/toggle_favorite_movie_response_state.dart';

class ToggleFavoriteMovie extends StatefulWidget {
  const ToggleFavoriteMovie(
      {@required this.bloc,
      @required this.movieId,
      @required this.iconSize,
      this.isFavorite = false,
      Key key})
      : assert(bloc != null),
        assert(movieId != null),
        assert(iconSize != null),
        super(key: key);

  final ToggleFavoriteMovieBloc bloc;
  final int movieId;
  final double iconSize;
  final bool isFavorite;

  @override
  _ToggleFavoriteMovieState createState() => _ToggleFavoriteMovieState();

  static Widget create({
    int movieId,
    double iconSize,
    bool isFavorite,
  }) =>
      ProxyProvider<MovieRepository, ToggleFavoriteMovieBloc>(
        update: (_, repository, __) => ToggleFavoriteMovieBloc(
          repository: repository,
          movieId: movieId,
          isFavoriteLastValue: isFavorite,
        ),
        dispose: (_, bloc) => bloc.dispose(),
        child: Consumer<ToggleFavoriteMovieBloc>(
          builder: (_, bloc, __) => ToggleFavoriteMovie(
            bloc: bloc,
            movieId: movieId,
            iconSize: iconSize,
          ),
        ),
      );
}

class _ToggleFavoriteMovieState extends State<ToggleFavoriteMovie> {
  ToggleFavoriteMovieBloc get _bloc => widget.bloc;

  double get _iconSize => widget.iconSize;

  @override
  Widget build(BuildContext context) => ActionHandler(
        onAction: (action) {
          if (action is DisplayFavoriteTogglingSuccess) {
            final snackBar = SnackBar(
              content: action.isFavorite
                  ? Text(S.of(context).toggleFavoriteSuccessAddMessage)
                  : Text(S.of(context).toggleFavoriteSuccessRemoveMessage),
            );
            Scaffold.of(context).showSnackBar(snackBar);
          } else {
            final snackBar = SnackBar(
              content: Text(S.of(context).generalExceptionMessage),
            );
            Scaffold.of(context).showSnackBar(snackBar);
          }
        },
        stream: _bloc.onNewAction,
        child: StreamBuilder(
          stream: _bloc.onNewState,
          builder: (_, snapshot) =>
              AsyncSnapshotResponseView<Loading, Error, Success>(
            snapshot: snapshot,
            successWidgetBuilder: (_, successState) => IconButton(
              icon: Icon(successState.isFavoritePreviousState
                  ? Icons.favorite
                  : Icons.favorite_border),
              onPressed: () => _bloc.onFavoriteMovie.add(null),
              iconSize: _iconSize,
              color: Colors.red,
            ),
            errorWidgetBuilder: (_, errorState) => IconButton(
              icon: Icon(errorState.isFavoritePreviousState
                  ? Icons.favorite
                  : Icons.favorite_border),
              onPressed: () => _bloc.onFavoriteMovie.add(null),
              iconSize: _iconSize,
              color: Colors.red,
            ),
          ),
        ),
      );
}
