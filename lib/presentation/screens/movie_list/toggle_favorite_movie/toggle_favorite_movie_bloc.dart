import 'dart:async';

import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:televi_flutter/data/repository/movie_repository.dart';
import 'package:televi_flutter/presentation/screens/movie_list/toggle_favorite_movie/toggle_favorite_movie_response_state.dart';

class ToggleFavoriteMovieBloc {
  ToggleFavoriteMovieBloc({
    @required this.repository,
    @required this.movieId,
    @required this.isFavoriteLastValue,
  })  : assert(repository != null),
        assert(movieId != null),
        assert(isFavoriteLastValue != null) {
    _subscriptions
      ..add(
        Stream.value(
          Success(isFavoriteLastValue),
        ).listen(_onNewStateSubject.add),
      )
      ..add(
        _onFavoriteMovieSubject.stream
            .flatMap(
              (_) => _favoriteMovie(),
            )
            .listen(_onNewStateSubject.add),
      );
  }

  final MovieRepository repository;
  final int movieId;
  bool isFavoriteLastValue;
  final _subscriptions = CompositeSubscription();
  final _onNewStateSubject =
      BehaviorSubject<ToggleFavoriteMovieResponseState>();
  final _onNewActionSubject =
      StreamController<ToggleFavoriteMovieResponseAction>();
  final _onFavoriteMovieSubject = StreamController<void>();

  Sink<void> get onFavoriteMovie => _onFavoriteMovieSubject.sink;

  Stream<ToggleFavoriteMovieResponseState> get onNewState => _onNewStateSubject;

  Stream<ToggleFavoriteMovieResponseAction> get onNewAction =>
      _onNewActionSubject.stream;

  Stream<ToggleFavoriteMovieResponseState> _favoriteMovie() async* {
    yield Loading();

    try {
      await repository.toggleFavoriteMovie(movieId);
      isFavoriteLastValue = !isFavoriteLastValue;
      yield Success(
        isFavoriteLastValue,
      );
      _onNewActionSubject.add(
        DisplayFavoriteTogglingSuccess(
          isFavoriteLastValue,
        ),
      );
    } catch (error) {
      yield Error(error, !isFavoriteLastValue);
      _onNewActionSubject.add(
        DisplayFavoriteTogglingError(error),
      );
    }
  }

  void dispose() {
    _onNewStateSubject.close();
    _onFavoriteMovieSubject.close();
    _onNewActionSubject.close();
    _subscriptions.dispose();
  }
}
