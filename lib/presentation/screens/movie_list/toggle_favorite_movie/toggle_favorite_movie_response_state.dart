abstract class ToggleFavoriteMovieResponseState {}

class Success implements ToggleFavoriteMovieResponseState {
  Success(this.isFavoritePreviousState);

  final bool isFavoritePreviousState;
}

class Loading implements ToggleFavoriteMovieResponseState {}

class Error implements ToggleFavoriteMovieResponseState {
  Error(this.error, this.isFavoritePreviousState);

  final dynamic error;
  final bool isFavoritePreviousState;
}

abstract class ToggleFavoriteMovieResponseAction {}

class DisplayFavoriteTogglingError
    implements ToggleFavoriteMovieResponseAction {
  DisplayFavoriteTogglingError(this.error);

  final dynamic error;
}

class DisplayFavoriteTogglingSuccess
    implements ToggleFavoriteMovieResponseAction {
  DisplayFavoriteTogglingSuccess(this.isFavorite);

  final bool isFavorite;
}
