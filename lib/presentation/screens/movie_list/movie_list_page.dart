import 'package:flutter/material.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:provider/provider.dart';
import 'package:televi_flutter/data/repository/movie_repository.dart';
import 'package:televi_flutter/generated/l10n.dart';
import 'package:televi_flutter/navigation/route_name_builder.dart';
import 'package:televi_flutter/presentation/common/async_snapshot_response_view.dart';
import 'package:televi_flutter/presentation/common/exception_indicator.dart';
import 'package:televi_flutter/presentation/screens/movie_list/movie_item.dart';
import 'package:televi_flutter/presentation/screens/movie_list/movie_list_bloc.dart';
import 'package:televi_flutter/presentation/screens/movie_list/movie_list_response_state.dart';

class MovieListPage extends StatefulWidget {
  const MovieListPage({@required this.bloc, Key key})
      : assert(bloc != null),
        super(key: key);
  final MovieListBloc bloc;

  @override
  _MovieListPageState createState() => _MovieListPageState();

  static Widget create() =>
      ProxyProvider<MovieRepository, MovieListBloc>(
        update: (_, repository, __) => MovieListBloc(
          repository: repository,
        ),
        dispose: (_, bloc) => bloc.dispose(),
        child: Consumer<MovieListBloc>(
          builder: (_, bloc, __) => MovieListPage(
            bloc: bloc,
          ),
        ),
      );
}

class _MovieListPageState extends State<MovieListPage> {
  // Vital for identifying our FocusDetector when a rebuild occurs.
  final Key _focusDetectorKey = UniqueKey();

  MovieListBloc get _bloc => widget.bloc;

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => FocusDetector(
        key: _focusDetectorKey,
        onFocusGained: () => _bloc.onFocusObtained.add(null),
        child: Scaffold(
          appBar: AppBar(
            title: Text(S.of(context).movieListAppBarTitle,
                style: Theme.of(context).textTheme.headline6),
          ),
          body: StreamBuilder(
            stream: _bloc.onNewState,
            builder: (_, snapshot) =>
                AsyncSnapshotResponseView<Loading, Error, Success>(
              snapshot: snapshot,
              onTryAgainTap: () => _bloc.onTryAgain.add(null),
              successWidgetBuilder: (_, successState) {
                final movieList = successState.movieList;
                return Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                  child: GridView.builder(
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3,
                            childAspectRatio: 0.7,
                            crossAxisSpacing: 10,
                            mainAxisSpacing: 15),
                    itemBuilder: (_, index) => MovieItem(
                      movie: movieList[index],
                      onMovieTap: navigateToDetails,
                    ),
                    itemCount: movieList.length,
                  ),
                );
              },
              errorWidgetBuilder: (_, errorState) => ExceptionIndicator(
                exception: errorState.error,
                onTapTryAgain: () => _bloc.onTryAgain.add(null),
              ),
            ),
          ),
        ),
      );

  void navigateToDetails(int id, String title) {
    Navigator.of(context).pushNamed(
      RouteNameBuilder.movieDetailPage(id, title),
    );
  }
}
