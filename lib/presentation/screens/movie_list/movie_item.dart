import 'package:flutter/material.dart';
import 'package:televi_flutter/models/movie_summary.dart';
import 'package:televi_flutter/presentation/screens/movie_list/toggle_favorite_movie/toggle_favorite_movie.dart';

typedef MovieTapCallback = void Function(int id, String name);

class MovieItem extends StatelessWidget {
  const MovieItem({
    @required this.movie,
    @required this.onMovieTap,
  })  : assert(movie != null),
        assert(onMovieTap != null);

  final MovieSummary movie;
  final MovieTapCallback onMovieTap;

  @override
  Widget build(BuildContext context) => ClipRRect(
        borderRadius: BorderRadius.circular(7),
        child: GestureDetector(
          onTap: () => onMovieTap(movie.id, movie.title),
          child: GridTile(
            footer: GridTileBar(
              title: ToggleFavoriteMovie.create(
                movieId: movie.id,
                iconSize: 35,
                isFavorite: movie.isFavorite,
              ),
              trailing: Text(
                '${movie.voteAverage}',
                style: const TextStyle(fontSize: 15, color: Colors.white),
              ),
              backgroundColor: Colors.black54,
            ),
            child: FadeInImage(
              alignment: Alignment.bottomCenter,
              placeholder: const AssetImage('assets/images/movie-icon.png'),
              image: NetworkImage(movie.posterUrl),
              fit: BoxFit.cover,
            ),
          ),
        ),
      );
}
