import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:rxdart/rxdart.dart';
import 'package:televi_flutter/data/repository/movie_repository.dart';
import 'package:televi_flutter/presentation/screens/movie_list/movie_list_response_state.dart';

class MovieListBloc {
  MovieListBloc({@required this.repository}) : assert(repository != null) {
    _subscriptions
      ..add(
        _fetchMovieSummaryList().listen(_onNewStateController.add),
      )
      ..add(
        Rx.merge([
          _onTryAgainController.stream,
          _onFocusObtainedController.stream
        ])
            .flatMap(
              (_) => _fetchMovieSummaryList(),
        )
            .listen(_onNewStateController.add),
      );
  }
  final MovieRepository repository;
  final _subscriptions = CompositeSubscription();
  final _onTryAgainController = StreamController<void>();
  final _onNewStateController =
      BehaviorSubject<MovieListResponseState>.seeded(
    Loading(),
  );
  final _onFocusObtainedController = StreamController<void>();


  Sink<void> get onTryAgain => _onTryAgainController.sink;

  Sink<void> get onFocusObtained => _onFocusObtainedController.sink;

  Stream<MovieListResponseState> get onNewState =>
      _onNewStateController.stream;

  Stream<MovieListResponseState> _fetchMovieSummaryList() async* {
    yield Loading();
    try {
      yield Success(
        movieList: await repository.getMovieList(),
      );
    } catch (error) {
      yield Error(error);
    }
  }

  void dispose() {
    _onTryAgainController.close();
    _onNewStateController.close();
    _onFocusObtainedController.close();
    _subscriptions.dispose();
  }
}
