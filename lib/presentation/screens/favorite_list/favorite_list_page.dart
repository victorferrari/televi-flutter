import 'package:flutter/material.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:provider/provider.dart';
import 'package:televi_flutter/data/repository/movie_repository.dart';
import 'package:televi_flutter/generated/l10n.dart';
import 'package:televi_flutter/presentation/common/async_snapshot_response_view.dart';
import 'package:televi_flutter/presentation/common/exception_indicator.dart';
import 'package:televi_flutter/presentation/screens/favorite_list/favorite_list_bloc.dart';
import 'package:televi_flutter/presentation/screens/favorite_list/favorite_list_item.dart';
import 'package:televi_flutter/presentation/screens/favorite_list/favorite_list_response_state.dart';

class FavoriteListPage extends StatefulWidget {
  const FavoriteListPage({@required this.bloc}) : assert(bloc != null);
  final FavoriteListBloc bloc;

  @override
  State<StatefulWidget> createState() => _FavoriteListPageState();

  static Widget create() =>
      ProxyProvider<MovieRepository, FavoriteListBloc>(
        update: (_, repository, __) => FavoriteListBloc(
          repository: repository,
        ),
        dispose: (_, bloc) => bloc.dispose(),
        child: Consumer<FavoriteListBloc>(
          builder: (_, bloc, __) => FavoriteListPage(
            bloc: bloc,
          ),
        ),
      );
}

class _FavoriteListPageState extends State<FavoriteListPage> {
  // Vital for identifying our FocusDetector when a rebuild occurs.
  final Key _focusDetectorKey = UniqueKey();

  FavoriteListBloc get _bloc => widget.bloc;

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => FocusDetector(
        key: _focusDetectorKey,
        onFocusGained: () => _bloc.onFocusObtained.add(null),
        child: Scaffold(
          appBar: AppBar(
            title: Text(S.of(context).favoriteListAppBarTitle,
                style: Theme.of(context).textTheme.headline6),
          ),
          body: StreamBuilder(
            stream: _bloc.onNewState,
            builder: (_, snapshot) =>
                AsyncSnapshotResponseView<Loading, Error, Success>(
              snapshot: snapshot,
              onTryAgainTap: () => _bloc.onTryAgain.add(null),
              successWidgetBuilder: (_, successState) {
                final favoriteList = successState.favoriteList;
                return Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                  child: ListView.builder(
                    itemBuilder: (_, index) => FavoriteMovieItem(
                      movie: favoriteList[index],
                    ),
                    itemCount: favoriteList.length,
                  ),
                );
              },
              errorWidgetBuilder: (_, errorState) => ExceptionIndicator(
                exception: errorState.error,
                onTapTryAgain: () => _bloc.onTryAgain.add(null),
              ),
            ),
          ),
        ),
      );
}
