import 'package:flutter/material.dart';
import 'package:televi_flutter/models/movie_summary.dart';

class FavoriteMovieItem extends StatelessWidget {
  const FavoriteMovieItem({
    @required this.movie,
  }) : assert(movie != null);

  final MovieSummary movie;

  @override
  Widget build(BuildContext context) => Container(
        decoration: const BoxDecoration(
          color: Color(0xFFECEFF1),
          borderRadius: BorderRadius.all(
            Radius.circular(5),
          ),
        ),
        margin: const EdgeInsets.only(bottom: 10),
        child: ListTile(
          leading: FadeInImage(
            placeholder: const AssetImage('assets/images/movie-icon.png'),
            image: NetworkImage(movie.posterUrl),
            fit: BoxFit.cover,
          ),
          title: Text(
            movie.title,
            style: Theme.of(context).textTheme.bodyText1,
          ),
          subtitle: Text(
            '${movie.voteAverage}',
            style: Theme.of(context).textTheme.bodyText2,
          ),
          trailing: const Icon(
            Icons.favorite,
            color: Colors.red,
            size: 35,
          ),
        ),
      );
}
