import 'package:flutter/material.dart';
import 'package:televi_flutter/models/movie_summary.dart';

abstract class FavoriteListResponseState {}

class Loading implements FavoriteListResponseState {}

class Success implements FavoriteListResponseState {
  Success({
    @required this.favoriteList,
  })  : assert(favoriteList != null);

  List<MovieSummary> favoriteList;
}

class Error implements FavoriteListResponseState {
  Error({
    @required this.error,
  })  : assert(error != null);

  dynamic error;
}
