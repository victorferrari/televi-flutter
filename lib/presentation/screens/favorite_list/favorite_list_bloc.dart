import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:rxdart/rxdart.dart';
import 'package:televi_flutter/data/repository/movie_repository.dart';
import 'package:televi_flutter/presentation/screens/favorite_list/favorite_list_response_state.dart';

class FavoriteListBloc {
  FavoriteListBloc({
    @required this.repository,
  }) : assert(repository != null) {
    _subscriptions
      ..add(
        _fetchFavoriteList().listen(_onNewStateController.add),
      )
      ..add(
        Rx.merge([
          _onTryAgainController.stream,
          _onFocusObtainedController.stream
        ])
            .flatMap(
              (_) => _fetchFavoriteList(),
            )
            .listen(_onNewStateController.add),
      );
  }

  final MovieRepository repository;
  final _subscriptions = CompositeSubscription();
  final _onTryAgainController = StreamController<void>();
  final _onFocusObtainedController = StreamController<void>();
  final _onNewStateController =
      BehaviorSubject<FavoriteListResponseState>.seeded(
    Loading(),
  );

  Sink<void> get onTryAgain => _onTryAgainController.sink;

  Sink<void> get onFocusObtained => _onFocusObtainedController.sink;

  Stream<FavoriteListResponseState> get onNewState =>
      _onNewStateController.stream;

  Stream<FavoriteListResponseState> _fetchFavoriteList() async* {
    yield Loading();
    try {
      yield Success(
        favoriteList: await repository.getFavoriteList(),
      );
    } catch (error) {
      yield Error(error: error);
    }
  }

  void dispose() {
    _onTryAgainController.close();
    _onFocusObtainedController.close();
    _onNewStateController.close();

    _subscriptions.dispose();
  }
}
