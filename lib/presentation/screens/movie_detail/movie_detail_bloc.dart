import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:rxdart/rxdart.dart';
import 'package:televi_flutter/data/repository/movie_repository.dart';
import 'package:televi_flutter/presentation/screens/movie_detail/movie_detail_response_state.dart';

class MovieDetailBloc {
  MovieDetailBloc({
    @required this.repository,
    @required this.movieId,
  })  : assert(repository != null),
        assert(movieId != null) {
    _subscriptions
      ..add(
        _fetchMovieDetail().listen(_onNewStateController.add),
      )
      ..add(
        _onTryAgainController.stream
            .flatMap(
              (_) => _fetchMovieDetail(),
            )
            .listen(_onNewStateController.add),
      );
  }

  final MovieRepository repository;
  final int movieId;
  final _subscriptions = CompositeSubscription();
  final _onTryAgainController = StreamController<void>();
  final _onNewStateController =
      BehaviorSubject<MovieDetailResponseState>.seeded(
    Loading(),
  );

  Sink<void> get onTryAgain => _onTryAgainController.sink;

  Stream<MovieDetailResponseState> get onNewState =>
      _onNewStateController.stream;

  Stream<MovieDetailResponseState> _fetchMovieDetail() async* {
    yield Loading();
    try {
      yield Success(
        movie: await repository.getMovieDetail(movieId),
      );
    } catch (error) {
      yield Error(error);
    }
  }

  void dispose() {
    _onTryAgainController.close();
    _onNewStateController.close();
    _subscriptions.dispose();
  }
}
