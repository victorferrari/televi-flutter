import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:televi_flutter/data/repository/movie_repository.dart';
import 'package:televi_flutter/presentation/common/async_snapshot_response_view.dart';
import 'package:televi_flutter/presentation/common/exception_indicator.dart';
import 'package:televi_flutter/presentation/screens/movie_detail/movie_detail_bloc.dart';
import 'package:televi_flutter/presentation/screens/movie_detail/movie_detail_item.dart';
import 'package:televi_flutter/presentation/screens/movie_detail/movie_detail_response_state.dart';

class MovieDetailPage extends StatefulWidget {
  const MovieDetailPage({
    @required this.bloc,
    @required this.movieId,
    @required this.movieTitle,
  })  : assert(bloc != null),
        assert(movieId != null),
        assert(movieTitle != null);

  final MovieDetailBloc bloc;
  final String movieTitle;
  final int movieId;

  @override
  _MovieDetailPageState createState() => _MovieDetailPageState();

  static Widget create({
    int movieId,
    String movieTitle,
  }) =>
      ProxyProvider<MovieRepository, MovieDetailBloc>(
        update: (_, repository, __) => MovieDetailBloc(
          repository: repository,
          movieId: movieId,
        ),
        dispose: (_, bloc) => bloc.dispose(),
        child: Consumer<MovieDetailBloc>(
          builder: (_, bloc, __) => MovieDetailPage(
            bloc: bloc,
            movieId: movieId,
            movieTitle: movieTitle,
          ),
        ),
      );
}

class _MovieDetailPageState extends State<MovieDetailPage> {
  MovieDetailBloc get _bloc => widget.bloc;

  String get _movieTitle => widget.movieTitle;

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(_movieTitle),
        ),
        body: StreamBuilder(
          stream: _bloc.onNewState,
          builder: (_, snapshot) =>
              AsyncSnapshotResponseView<Loading, Error, Success>(
            snapshot: snapshot,
            onTryAgainTap: () => _bloc.onTryAgain.add(null),
            successWidgetBuilder: (_, successState) =>
                MovieDetailItem(movie: successState.movie),
            errorWidgetBuilder: (_, errorState) => ExceptionIndicator(
              exception: errorState.error,
              onTapTryAgain: () => _bloc.onTryAgain.add(null),
            ),
          ),
        ),
      );
}
