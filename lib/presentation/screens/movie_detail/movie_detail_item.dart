import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:televi_flutter/generated/l10n.dart';
import 'package:televi_flutter/models/movie_detail.dart';

class MovieDetailItem extends StatelessWidget {
  MovieDetailItem({@required this.movie}) : assert(movie != null);
  final MovieDetail movie;
  final RegExp _regexp = RegExp('[\\[\\]]');

  @override
  Widget build(BuildContext context) {
    final dateFormat = DateFormat(S.of(context).dateFormat);

    return SingleChildScrollView(
      child: Column(
        children: [
          FadeInImage(
            placeholder: const AssetImage('assets/images/movie-icon.png'),
            image: NetworkImage(movie.posterUrl),
            fit: BoxFit.fill,
            height: 200,
            width: MediaQuery.of(context).size.width,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _MovieSectionItem(
                  title: S.of(context).movieDetailTitleLabel,
                  information: movie.title,
                  isLastSection: false,
                ),
                const SizedBox(height: 10),
                _MovieSectionItem(
                  title: S.of(context).movieDetailOverviewLabel,
                  information: movie.overview,
                  isLastSection: false,
                ),
                const SizedBox(height: 10),
                _MovieSectionItem(
                  title: S.of(context).movieDetailGenresLabel,
                  information: movie.genres.toString().replaceAll(_regexp, ''),
                  isLastSection: false,
                ),
                const SizedBox(height: 10),
                _MovieSectionItem(
                  title: S.of(context).movieDetailReleaseDateLabel,
                  information: dateFormat.format(
                    DateTime.parse(movie.releaseDate),
                  ),
                  isLastSection: false,
                ),
                const SizedBox(height: 10),
                _MovieSectionItem(
                  title: S.of(context).movieDetailRuntimeLabel,
                  information:
                      S.of(context).movieDetailRuntimeText(movie.runtime),
                  isLastSection: false,
                ),
                const SizedBox(height: 10),
                _MovieSectionItem(
                  title: S.of(context).movieDetailVoteAverageLabel,
                  information: movie.voteAverage.toString(),
                  isLastSection: true,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _MovieSectionItem extends StatelessWidget {
  const _MovieSectionItem(
      {@required this.title,
      @required this.information,
      @required this.isLastSection})
      : assert(title != null),
        assert(information != null),
        assert(isLastSection != null);

  final String title;
  final String information;
  final bool isLastSection;

  @override
  Widget build(BuildContext context) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: Theme.of(context).textTheme.bodyText2,
            textAlign: TextAlign.start,
          ),
          const SizedBox(height: 5),
          Text(
            information,
            style: Theme.of(context).textTheme.bodyText1,
            textAlign: TextAlign.start,
          ),
          const SizedBox(height: 10),
          if (!isLastSection)
            const Divider(
              color: Colors.blueGrey,
              height: 1,
            ),
        ],
      );
}
