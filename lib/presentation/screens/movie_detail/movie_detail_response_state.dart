import 'package:flutter/widgets.dart';
import 'package:televi_flutter/models/movie_detail.dart';

abstract class MovieDetailResponseState {}

class Loading implements MovieDetailResponseState {}

class Success implements MovieDetailResponseState {
  Success({@required this.movie}) : assert(movie != null);

  MovieDetail movie;
}

class Error implements MovieDetailResponseState {
  Error(this.error) : assert(error != null);

  dynamic error;
}