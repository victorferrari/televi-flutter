import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:televi_flutter/navigation/material_tab_view.dart';
import 'package:televi_flutter/navigation/route_name_builder.dart';
import 'package:televi_flutter/presentation/screens/home/bottom_navigation/bottom_navigation_tab.dart';
import 'package:televi_flutter/presentation/screens/home/bottom_navigation/material/material_bottom_navigation_tab.dart';
import 'package:fluro/fluro.dart';

Widget buildMaterialPageFlow(
    MaterialBottomNavigationTab barItem,
    int selectedIndex,
    int tabIndex,
    List<bool> shouldBuildTab,
    List<AnimationController> animationControllers) {
  final isCurrentlySelected = tabIndex == selectedIndex;

  // We should build the tab content only if it was already built or
  // if it is currently selected.
  shouldBuildTab[tabIndex] = isCurrentlySelected || shouldBuildTab[tabIndex];

  final view = MaterialTabView(
    barItem: barItem,
    tabIndex: tabIndex,
    shouldBuildTab: shouldBuildTab,
    animationControllers: animationControllers,
  );

  if (tabIndex == selectedIndex) {
    animationControllers[tabIndex].forward();
    return view;
  } else {
    animationControllers[tabIndex].reverse();
    if (animationControllers[tabIndex].isAnimating) {
      return IgnorePointer(child: view);
    }
    return Offstage(child: view);
  }
}

Widget buildCupertinoPageFlow(
        BottomNavigationTab barItem, BuildContext context) =>
    CupertinoTabView(
      navigatorKey: barItem.navigatorKey,
      onGenerateRoute: (settings) {
        var routeSettings = settings;
        if (settings.name == RouteNameBuilder.movieListPath) {
          routeSettings = settings.copyWith(name: barItem.initialRouteName);
        }
        return FluroRouter.appRouter
            .matchRoute(
              context,
              routeSettings.name,
              routeSettings: routeSettings,
            )
            .route;
      },
    );
