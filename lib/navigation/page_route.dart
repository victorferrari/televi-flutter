import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

PageRoute<T> buildAdaptivePageRoute<T>({
  @required WidgetBuilder builder,
  bool fullscreenDialog = false,
}) =>
    Platform.isAndroid
        ? MaterialPageRoute(
            builder: builder,
            fullscreenDialog: fullscreenDialog,
          )
        : CupertinoPageRoute(
            builder: builder,
            fullscreenDialog: fullscreenDialog,
          );
