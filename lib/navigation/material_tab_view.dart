import 'package:flutter/material.dart';
import 'package:televi_flutter/navigation/route_name_builder.dart';
import 'package:televi_flutter/presentation/screens/home/bottom_navigation/material/material_bottom_navigation_tab.dart';
import 'package:fluro/fluro.dart';

class MaterialTabView extends StatelessWidget {
  const MaterialTabView({
    this.barItem,
    this.tabIndex,
    this.shouldBuildTab,
    this.animationControllers,
  });

  final MaterialBottomNavigationTab barItem;
  final int tabIndex;
  final List<bool> shouldBuildTab;
  final List<AnimationController> animationControllers;

  @override
  Widget build(BuildContext context) => FadeTransition(
        opacity: animationControllers[tabIndex].drive(
          CurveTween(curve: Curves.fastOutSlowIn),
        ),
        child: KeyedSubtree(
          key: barItem.subtreeKey,
          child: shouldBuildTab[tabIndex]
              ? Navigator(
                  // The key enables us to access the Navigator's
                  // state inside the onWillPop callback and for
                  // emptying its stack when a tab is re-selected.
                  // That is why a GlobalKey is needed instead of
                  // a simpler ValueKey.
                  key: barItem.navigatorKey,
                  initialRoute: barItem.initialRouteName,
                  onGenerateRoute: (settings) => FluroRouter.appRouter
                      .matchRoute(context, settings.name,
                          routeSettings: settings)
                      .route)
              : Container(),
        ),
      );
}
