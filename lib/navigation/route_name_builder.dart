class RouteNameBuilder {
  /* ---   Paths    --- */
  static const homePath = '/homeScreen';
  static const movieListPath = '/movieListPage';
  static const movieDetailPath = '/movieDetailPage';
  static const favoriteListPath = '/favoriteListPage';

  /* ---   Params    --- */
  static const movieDetailIdParameter = 'id';
  static const movieDetailTitleParameter = 'title';

  static String homeScreen() => '$homePath';

  static String movieListPage() => '$movieListPath';

  static String movieDetailPage(int id, String title) =>
      '$movieDetailPath/$id?$movieDetailTitleParameter=$title';

  static String favoriteListPage() => '$favoriteListPath';
}
