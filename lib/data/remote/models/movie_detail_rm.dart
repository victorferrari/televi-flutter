import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'movie_detail_rm.g.dart';

@JsonSerializable(nullable: false)
class MovieDetailRM {
  MovieDetailRM({
    @required this.id,
    @required this.voteAverage,
    @required this.title,
    @required this.genres,
    @required this.releaseDate,
    @required this.runtime,
    @required this.overview,
    @required this.posterUrl,
  })  : assert(id != null),
        assert(voteAverage != null),
        assert(title != null),
        assert(genres != null),
        assert(releaseDate != null),
        assert(runtime != null),
        assert(overview != null),
        assert(posterUrl != null);

  factory MovieDetailRM.fromJson(Map<String, dynamic> json) =>
      _$MovieDetailRMFromJson(json);

  final int id;
  @JsonKey(name: 'vote_average')
  final double voteAverage;
  final String title;
  @JsonKey(name: 'poster_url')
  final String posterUrl;
  final List<String> genres;
  @JsonKey(name: 'release_date')
  final String releaseDate;
  final int runtime;
  final String overview;
}
