// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_detail_rm.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MovieDetailRM _$MovieDetailRMFromJson(Map<String, dynamic> json) {
  return MovieDetailRM(
    id: json['id'] as int,
    voteAverage: (json['vote_average'] as num).toDouble(),
    title: json['title'] as String,
    genres: (json['genres'] as List).map((e) => e as String).toList(),
    releaseDate: json['release_date'] as String,
    runtime: json['runtime'] as int,
    overview: json['overview'] as String,
    posterUrl: json['poster_url'] as String,
  );
}

Map<String, dynamic> _$MovieDetailRMToJson(MovieDetailRM instance) =>
    <String, dynamic>{
      'id': instance.id,
      'vote_average': instance.voteAverage,
      'title': instance.title,
      'poster_url': instance.posterUrl,
      'genres': instance.genres,
      'release_date': instance.releaseDate,
      'runtime': instance.runtime,
      'overview': instance.overview,
    };
