import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'movie_summary_rm.g.dart';

@JsonSerializable(nullable: false)
class MovieSummaryRM {
  MovieSummaryRM({
    @required this.id,
    @required this.voteAverage,
    @required this.title,
    @required this.posterUrl,
  })  : assert(id != null),
        assert(voteAverage != null),
        assert(title != null),
        assert(posterUrl != null);

  factory MovieSummaryRM.fromJson(Map<String, dynamic> json) =>
      _$MovieSummaryRMFromJson(json);

  final int id;
  @JsonKey(name: 'vote_average')
  final double voteAverage;
  final String title;
  @JsonKey(name: 'poster_url')
  final String posterUrl;
}
