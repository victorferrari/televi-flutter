// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_summary_rm.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MovieSummaryRM _$MovieSummaryRMFromJson(Map<String, dynamic> json) {
  return MovieSummaryRM(
    id: json['id'] as int,
    voteAverage: (json['vote_average'] as num).toDouble(),
    title: json['title'] as String,
    posterUrl: json['poster_url'] as String,
  );
}

Map<String, dynamic> _$MovieSummaryRMToJson(MovieSummaryRM instance) =>
    <String, dynamic>{
      'id': instance.id,
      'vote_average': instance.voteAverage,
      'title': instance.title,
      'poster_url': instance.posterUrl,
    };
