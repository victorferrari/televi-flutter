class Urls {
  static const _baseUrl = 'https://desafio-mobile.nyc3.digitaloceanspaces.com/';
  static const _movies = '${_baseUrl}movies';

  static String movieList() => '$_movies';
  static String movieDetail(int movieId) => '$_movies/$movieId';
}


