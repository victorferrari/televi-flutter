import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:televi_flutter/data/remote/exceptions.dart';
import 'package:televi_flutter/data/remote/models/movie_detail_rm.dart';
import 'package:televi_flutter/data/remote/models/movie_summary_rm.dart';
import 'package:televi_flutter/data/remote/url_strings.dart';

class MovieRemoteDataSource {
  MovieRemoteDataSource({
    @required this.dio,
  }) : assert(dio != null);

  final Dio dio;

  Future<List<MovieSummaryRM>> getMovieList() => dio
      .get(Urls.movieList())
      .then(
        (response) => List.from(
          jsonDecode(
            jsonEncode(response.data),
          ),
        ),
      )
      .then(
        (movieList) => movieList
            .map(
              (movie) => MovieSummaryRM.fromJson(movie),
            )
            .toList(),
      )
      .catchError(
        _throwError,
      );

  Future<MovieDetailRM> getMovieDetail(int movieId) => dio
      .get(
        Urls.movieDetail(movieId),
      )
      .then(
        (response) => jsonDecode(
          jsonEncode(response.data),
        ),
      )
      .then(
        (movie) => MovieDetailRM.fromJson(movie),
      )
      .catchError(
        _throwError,
      );

  void _throwError(dynamic error) {
    if (error is DioError && error.error is SocketException) {
      throw NoInternetException();
    } else {
      throw GeneralException();
    }
  }
}
