import 'package:televi_flutter/data/cache/models/movie_detail_cm.dart';
import 'package:televi_flutter/data/cache/models/movie_summary_cm.dart';
import 'package:televi_flutter/data/remote/models/movie_detail_rm.dart';
import 'package:televi_flutter/data/remote/models/movie_summary_rm.dart';

extension MovieSummaryRMtoCM on MovieSummaryRM {
  MovieSummaryCM toCacheModel() => MovieSummaryCM(
        id: id,
        voteAverage: voteAverage,
        title: title,
        posterUrl: posterUrl,
      );
}

extension MovieDetailRMtoCM on MovieDetailRM {
  MovieDetailCM toCacheModel() => MovieDetailCM(
      id: id,
      voteAverage: voteAverage,
      title: title,
      genres: genres,
      releaseDate: releaseDate,
      runtime: runtime,
      overview: overview,
      posterUrl: posterUrl);
}
