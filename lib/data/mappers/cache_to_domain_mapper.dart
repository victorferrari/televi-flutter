import 'package:televi_flutter/data/cache/models/movie_detail_cm.dart';
import 'package:televi_flutter/data/cache/models/movie_summary_cm.dart';
import 'package:televi_flutter/models/movie_detail.dart';
import 'package:televi_flutter/models/movie_summary.dart';

extension MovieSummaryCMtoDM on MovieSummaryCM {
  MovieSummary toDomainModel(bool isFavorite) => MovieSummary(
    id: id,
    voteAverage: voteAverage,
    title: title,
    posterUrl: posterUrl,
    isFavorite: isFavorite,
  );
}

extension MovieDetailCMtoDM on MovieDetailCM {
  MovieDetail toDomainModel() => MovieDetail(
      id: id,
      voteAverage: voteAverage,
      title: title,
      genres: genres,
      releaseDate: releaseDate,
      runtime: runtime,
      overview: overview,
      posterUrl: posterUrl);
}