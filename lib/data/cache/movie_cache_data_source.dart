import 'package:hive/hive.dart';
import 'package:televi_flutter/data/cache/models/movie_detail_cm.dart';
import 'package:televi_flutter/data/cache/models/movie_summary_cm.dart';

class MovieCacheDataSource {
  static const _favoriteListKey = 'favoriteList';
  static const _movieListKey = 'movieList';
  static const _movieDetailKey = 'movieDetail';

  Future<List<MovieSummaryCM>> getMovieList() =>
      Hive.openBox<List<MovieSummaryCM>>(_movieListKey)
          .then(
            (box) => List<MovieSummaryCM>.from(
              box.get(
                0,
              ),
            ),
          )
          .then(
            (movieListCM) => movieListCM.verifyContent(),
          );

  Future<void> upsertMovieList(List<MovieSummaryCM> movieList) =>
      Hive.openBox<List<MovieSummaryCM>>(_movieListKey).then(
        (box) => box.put(
          0,
          movieList,
        ),
      );

  Future<List<int>> getFavoriteMovieIdList() =>
      Hive.openBox<List<int>>(_favoriteListKey).then(
        (box) => List.from(
          box.get(
            0,
            defaultValue: List.empty(),
          ),
        ),
      );

  Future<void> toggleFavoriteMovie(int movieId) =>
      Hive.openBox<List<int>>(_favoriteListKey).then(
        (box) => getFavoriteMovieIdList().then(
          (favoriteList) {
            favoriteList.contains(movieId)
                ? favoriteList.remove(movieId)
                : favoriteList.add(movieId);
            box.put(
              0,
              favoriteList,
            );
          },
        ),
      );

  Future<MovieDetailCM> getMovie(int movieId) =>
      Hive.openBox<MovieDetailCM>(_movieDetailKey)
          .then(
            (box) => box.get(
              movieId,
            ),
          )
          .then(
            (movieDetailCM) => movieDetailCM.verifyContent(),
          );

  Future<void> upsertMovie(MovieDetailCM movie) =>
      Hive.openBox<MovieDetailCM>(_movieDetailKey).then(
        (box) => box.put(movie.id, movie),
      );
}

extension on dynamic {
  dynamic verifyContent() {
    if (this == null) {
      throw EmptyCacheException();
    } else {
      return this;
    }
  }
}

class EmptyCacheException implements Exception {}
