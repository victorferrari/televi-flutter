// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_detail_cm.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MovieDetailCMAdapter extends TypeAdapter<MovieDetailCM> {
  @override
  final int typeId = 1;

  @override
  MovieDetailCM read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MovieDetailCM(
      id: fields[0] as int,
      voteAverage: fields[1] as double,
      title: fields[2] as String,
      genres: (fields[4] as List)?.cast<String>(),
      releaseDate: fields[5] as String,
      runtime: fields[6] as int,
      overview: fields[7] as String,
      posterUrl: fields[3] as String,
    );
  }

  @override
  void write(BinaryWriter writer, MovieDetailCM obj) {
    writer
      ..writeByte(8)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.voteAverage)
      ..writeByte(2)
      ..write(obj.title)
      ..writeByte(3)
      ..write(obj.posterUrl)
      ..writeByte(4)
      ..write(obj.genres)
      ..writeByte(5)
      ..write(obj.releaseDate)
      ..writeByte(6)
      ..write(obj.runtime)
      ..writeByte(7)
      ..write(obj.overview);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MovieDetailCMAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
