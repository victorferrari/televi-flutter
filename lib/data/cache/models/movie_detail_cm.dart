import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

part 'movie_detail_cm.g.dart';

@HiveType(typeId: 1)
class MovieDetailCM {
  MovieDetailCM({
    @required this.id,
    @required this.voteAverage,
    @required this.title,
    @required this.genres,
    @required this.releaseDate,
    @required this.runtime,
    @required this.overview,
    @required this.posterUrl,
  })  : assert(id != null),
        assert(voteAverage != null),
        assert(title != null),
        assert(genres != null),
        assert(releaseDate != null),
        assert(runtime != null),
        assert(overview != null),
        assert(posterUrl != null);

  @HiveField(0)
  final int id;
  @HiveField(1)
  final double voteAverage;
  @HiveField(2)
  final String title;
  @HiveField(3)
  final String posterUrl;
  @HiveField(4)
  final List<String> genres;
  @HiveField(5)
  final String releaseDate;
  @HiveField(6)
  final int runtime;
  @HiveField(7)
  final String overview;
}
