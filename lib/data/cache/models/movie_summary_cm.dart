import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

part 'movie_summary_cm.g.dart';

@HiveType(typeId: 0)
class MovieSummaryCM {
  MovieSummaryCM({
    @required this.id,
    @required this.voteAverage,
    @required this.title,
    @required this.posterUrl,
  })  : assert(id != null),
        assert(voteAverage != null),
        assert(title != null),
        assert(posterUrl != null);

  @HiveField(0)
  final int id;
  @HiveField(1)
  final double voteAverage;
  @HiveField(2)
  final String title;
  @HiveField(3)
  final String posterUrl;
}
