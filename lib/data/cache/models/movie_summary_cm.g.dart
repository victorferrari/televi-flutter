// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_summary_cm.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MovieSummaryCMAdapter extends TypeAdapter<MovieSummaryCM> {
  @override
  final int typeId = 0;

  @override
  MovieSummaryCM read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MovieSummaryCM(
      id: fields[0] as int,
      voteAverage: fields[1] as double,
      title: fields[2] as String,
      posterUrl: fields[3] as String,
    );
  }

  @override
  void write(BinaryWriter writer, MovieSummaryCM obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.voteAverage)
      ..writeByte(2)
      ..write(obj.title)
      ..writeByte(3)
      ..write(obj.posterUrl);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MovieSummaryCMAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
