import 'package:flutter/cupertino.dart';
import 'package:televi_flutter/data/cache/models/movie_summary_cm.dart';
import 'package:televi_flutter/data/cache/movie_cache_data_source.dart';
import 'package:televi_flutter/data/mappers/cache_to_domain_mapper.dart';
import 'package:televi_flutter/data/mappers/remote_to_cache_mapper.dart';
import 'package:televi_flutter/data/remote/movie_remote_data_source.dart';
import 'package:televi_flutter/models/movie_detail.dart';
import 'package:televi_flutter/models/movie_summary.dart';

class MovieRepository {
  MovieRepository({
    @required this.remote,
    @required this.cache,
  })  : assert(remote != null),
        assert(cache != null);

  final MovieRemoteDataSource remote;
  final MovieCacheDataSource cache;

  Future<List<MovieSummary>> getMovieList() => Future.wait([
        remote
            .getMovieList()
            .then(
              (movieListRM) => movieListRM
                  .map(
                    (movieRM) => movieRM.toCacheModel(),
                  )
                  .toList(),
            )
            .then((movieListCM) =>
                cache.upsertMovieList(movieListCM).then((_) => movieListCM))
            .catchError(
              (remoteError) => cache.getMovieList().catchError(
                (_) {
                  throw remoteError;
                },
              ),
            ),
        cache.getFavoriteMovieIdList(),
      ]).then(
        (result) {
          final List<MovieSummaryCM> movieListCM = result[0];
          final List<int> favoriteList = result[1];
          return movieListCM
              .map(
                (movieCM) => movieCM.toDomainModel(
                  favoriteList.contains(movieCM.id),
                ),
              )
              .toList();
        },
      );

  Future<List<MovieSummary>> getFavoriteList() => getMovieList().then(
        (movieList) => movieList.where((movie) => movie.isFavorite).toList(),
      );

  Future<void> toggleFavoriteMovie(int movieId) =>
      cache.toggleFavoriteMovie(movieId);

  Future<MovieDetail> getMovieDetail(int movieId) => remote
      .getMovieDetail(movieId)
      .then(
        (movieRM) => movieRM.toCacheModel(),
      )
      .then(
        (movieCM) => cache.upsertMovie(movieCM).then((_) => movieCM),
      )
      .catchError(
        (remoteError) => cache.getMovie(movieId).catchError(
          (_) {
            throw remoteError;
          },
        ),
      )
      .then(
        (movieCM) => movieCM.toDomainModel(),
      );
}
